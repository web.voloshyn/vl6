import RSingleNews from '@/views/news/SingleNews.vue'

export default [
    {
        path: '/r-news-home',
        name: 'r-news-home',
        components: {
            default: () => import('@/views/news/NewsHome.vue'),
            addNav: () => import('@/views/news/Nav.vue')
        }
    },
    {
        path: '/r-news-sep',
        name: 'r-news',
        components: {
            default: () => import('@/views/news/News.vue'),
            addNav: () => import('@/views/news/Nav.vue')
        }
    },

    // /:id  - диннамический путь
    // для роута параметры можно передать в виде props, в конфиге роута добавляется свойство props: true,
    // а в компоненте представления регистрируется props с соответствующему параметру названием
    // для маршрутов с именнованным представлением, нужно указывать опцию пропс для каждого именованного представления
    {
        path: '/r-news-sep/:id',
        name: 'r-single-news',
        props: { default: true, addNav: false },
        components: {
            default: RSingleNews,
            addNav: () => import('@/views/news/Nav.vue')
        }
    },
    {
        path: '/r-news-with',
        name: 'r-news-with',
        components: {
            default: () => import('@/views/news/NewsWithSingle.vue'),
            addNav: () => import('@/views/news/Nav.vue')
        },
        children: [
            /* Вложенные маршруты - в родительском компоненте должен быть router-view
            для отображения компонента дочернего роута(компонента)
            children - принимает массив роутов, в теории количество роутов не ограничено*/
            {
                name: 'news-with-single',
                // так как этот роут потомок, он унаследует путь от родителя - /r-news-with/:id
                path: ':id',
                props: true,
                component: RSingleNews
            }
        ]
    },
]