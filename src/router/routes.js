
import RHome from '@/views/router/Home.vue'


export default [

    {
        path: '/r-home',
        name: 'r-home',
        // redirect: 'path', {name: ''}, (to) => {должно вернуть строку или обьект пути}
        alias: '/r-home-page',
        /*Для использования именованного представления, нужно использовать свойство "components"*/
        components: {
            // default - router-view без имени
            // идентично слотам
            default: RHome,
            addNav: () => import('@/views/router/Nav.vue')
        }
    },
    {
        path: '/r-about',
        name: 'r-about',
        components: {
            default: () => import('@/views/router/About.vue'),
            addNav: () => import('@/views/router/Nav.vue')
        }
    },
    {
        path: '/r-hooks',
        name: 'r-hooks',
        components: {
            default: () => import('@/views/router/LifeHooks.vue'),
            addNav: () => import('@/views/router/Nav.vue')
        }
    }

]